package PT2019.demo.Tema2;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
class App 
{
	
	Queue c = new Queue("Coada 1");
	Queue c2 = new Queue("Coada 2");
	Queue c3 = new Queue("Coada 3");
	Queue c4 = new Queue("Coada 4");
	Queue c5 = new Queue("Coada 5");
	
	int coada_minim()
	{		
		if(c.size <= c2.size && c.size <= c3.size && c.size <= c4.size && c.size <= c5.size)
			return 1;
		if(c2.size <= c.size && c2.size <= c3.size && c2.size <= c4.size && c2.size <= c5.size)
			return 2;
		if(c3.size <= c2.size && c3.size <= c.size && c3.size <= c4.size && c3.size <= c5.size)
			return 3;
		if(c4.size <= c.size && c4.size <= c3.size && c4.size <= c2.size && c4.size <= c5.size)
			return 4;
		if(c5.size <= c2.size && c5.size <= c3.size && c5.size <= c4.size && c5.size <= c.size)
			return 5;
		return 1;
	}
	
	class Tred implements Runnable
	{

		public void run() 
		{
			while(true)
			{
				
				int coada = coada_minim();
				Client cris = new Client();
				System.out.println("Client nou cu " + cris.items + " servicii necesare adaugat in coada "+ coada);
			
				if(coada == 1)
					c.add(cris);
				else if (coada == 2)
					c2.add(cris);
				else if (coada ==3)
					c3.add(cris);
				else if (coada ==4)
					c4.add(cris);
				else 
					c5.add(cris);
				Random rand = new Random();
				int n = rand.nextInt(2000)+500;
				try {Thread.sleep(n);} catch (InterruptedException e) {}
				}
				
			}		
		}
		
	
	class Tred2 implements Runnable
	{
		public void run() 
		{
			while(true)
			{	
				if(c.isEmpty()==false)
				{
					Client aux = c.queue[0];
					int duration = aux.items * c.service_speed;
					
					try{Thread.sleep(duration);}catch(Exception e) {}
					System.out.println("Client extras de la coada 1 dupa " + duration + " milisecunde");
					c.take();
				}
				else {try{Thread.sleep(1);}catch(Exception e) {}}
				
				
			}
		}
	}
	
	class Tred2c2 implements Runnable
	{
		public void run() 
		{
			while(true)
			{	
				if(c2.isEmpty()==false)
				{
					Client aux = c2.queue[0];
					int duration = aux.items * c2.service_speed;
					
					try{Thread.sleep(duration);}catch(Exception e) {}
					System.out.println("Client extras de la coada 2 dupa " + duration + " milisecunde");
					c2.take();
				}
				else {try{Thread.sleep(1);}catch(Exception e) {}}
				
			}
		}
	}
	
	class Tred2c3 implements Runnable
	{
		public void run() 
		{
			while(true)
			{	
				if(c3.isEmpty()==false)
				{
					Client aux3 = c3.queue[0];
					int duration3 = aux3.items * c3.service_speed;
					
					try{Thread.sleep(duration3);}catch(Exception e) {}
					System.out.println("Client extras de la coada 3 dupa " + duration3 + "milisecunde");
					c3.take();
				}
				else {try{Thread.sleep(1);}catch(Exception e) {}}
				
			}
		}
	}
	
	class Tred2c4 implements Runnable
	{
		public void run() 
		{
			while(true)
			{	
				if(c4.isEmpty()==false)
				{
					Client aux3 = c4.queue[0];
					int duration3 = aux3.items * c4.service_speed;
					
					try{Thread.sleep(duration3);}catch(Exception e) {}
					System.out.println("Client extras de la coada 4 dupa " + duration3 + "milisecunde");
					c4.take();
				}
				else {try{Thread.sleep(1);}catch(Exception e) {}}
				
			}
		}
	}
	
	class Tred2c5 implements Runnable
	{
		public void run() 
		{
			while(true)
			{
				if(c5.isEmpty()==false)
				{
					Client aux3 = c5.queue[0];
					int duration3 = aux3.items * c5.service_speed;
					
					try{Thread.sleep(duration3);}catch(Exception e) {}
					System.out.println("Client extras de la coada 5 dupa " + duration3 + "milisecunde");
					c5.take();
				}
				else {try{Thread.sleep(1);}catch(Exception e) {}}
				
			}
		}
	}
	
	


	int nr_max = 0;
	class Grafic extends JPanel implements ActionListener
	{
		private static final long serialVersionUID = 1L;
		Timer t = new Timer(5, this);
		int y;
		int coada;
		public void paintComponent(Graphics g)
		{
			int nr_clienti = 0;
			if(coada == 1)
				 nr_clienti= c.size;
			else if (coada == 2)
				 nr_clienti= c2.size;
			else if (coada == 3)
				 nr_clienti= c3.size;
			else if (coada == 4)
				 nr_clienti= c4.size;
			else if (coada == 5)
				 nr_clienti= c5.size;
			
			if(nr_clienti > nr_max)
				nr_max = nr_clienti;
			int x = 100;
			for(int i=0; i< nr_max; i++)
			{
				g.clearRect(x,y, 40, 40);
				x+=50;
			}
			x=100;
			for(int i=0; i< nr_clienti; i++)
			{
				g.fillRect(x, y, 40, 40);
				x+=50;
			}
			t.start();
		}
		
		public void actionPerformed(ActionEvent arg0)
		{
			repaint();
		} 
	}

	void adevaratul_main()
	{
		JFrame f1 = new JFrame();
		JPanel p = new JPanel();
		Tred obj1 = new Tred();
		//Tredc2 a = new Tredc2();
		//Tredc3 b = new Tredc3();
		//Tredc4 c = new Tredc4();
		//Tredc5 d = new Tredc5();		
		
		
    	Tred2 obj2 = new Tred2();
    	Tred2c2 aa = new Tred2c2();
    	Tred2c3 ab = new Tred2c3();
    	Tred2c4 ac = new Tred2c4();
    	Tred2c5 ad = new Tred2c5();
    	
    	
    	Thread bec = new Thread(obj1);
        Thread bec2 = new Thread(obj2);
       // Thread t1 = new Thread(a);
        //Thread t2 = new Thread(b);
        //Thread t3 = new Thread(c);
        //Thread t4 = new Thread(d);
        
        Thread t5 = new Thread(aa);
        Thread t6 = new Thread(ab);
        Thread t7 = new Thread(ac);
        Thread t8 = new Thread(ad);
        
        
        Grafic o = new Grafic();
        Grafic o2 = new Grafic();
        Grafic o3 = new Grafic();
        Grafic o4 = new Grafic();
        Grafic o5 = new Grafic();
        
        o.y = 50; o.coada = 1;
        o2.y = 100; o2.coada = 2;
        o3.y = 150; o3.coada = 3;
        o4.y = 200; o4.coada = 4;
        o5.y = 250; o5.coada = 5;
  
        
   
	    bec.start();
	    bec2.start();
	   // bec3.start();
	    //t1.start();
	    //t2.start();
	    //t3.start();
	    //t4.start();
	   
	    t5.start();
	    t6.start();
	    t7.start();
	    t8.start();
	    
	    p.setLayout(new GridLayout(2,1));
       
	    
	    p.setBackground(Color.WHITE);
        p.add(o);
        p.add(o2);
        p.add(o3);
        p.add(o4);
        p.add(o5);
        f1.setContentPane(p);
	    
        f1.setSize(1100, 800);
        f1.setVisible(true);
	    
	}

    public static void main( String[] args ) 
    {
    	App da = new App();
    	da.adevaratul_main();
        
    }
}

